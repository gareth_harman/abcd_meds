---
title: "Parse ABCD Medications for Categories"
author: "Sara Shao"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# load packages
library(tidyverse)
```

# Pre-Tagging

```{r}
# load med data
ph_p_meds <- read_csv("~/Documents/ABCD Data Releases/abcd-data-release-5.1/core/physical-health/ph_p_meds.csv")
```

Columns w/ suffix 'rxnorm_p' and 'rx_norm_1yr_p' contain medication names taken by the participant in the past 2 weeks (before Y3) and past year (Y3 and after) respectively.

```{r}
# select columns with medications names
med_names <- ph_p_meds %>%
  select(src_subject_id, eventname, contains('_rxnorm_p'), contains('_rxnorm_1yr_p')) %>%
  select(-contains('otc'))
```

```{r}
# save as RDS
saveRDS(med_names, 'meds.RDS')
```

Next, run parse_meds.R >>

```{r}
# load parse_meds.R output file
medication_tagging <- read_csv("medication_tagging.csv")
```

```{r}
# check for errors, make sure there are no letters in the med_id column
med_tagging2 <- medication_tagging %>%
  mutate(med_id = str_remove(med_id, 'T')) %>%
  slice(-85) # row contained weird javascript stuff instead of med
```

```{r}
# save file again
write_csv(med_tagging2, 'medication_tagging.csv')
```

Next, run restful_get_rxnorm.py to tag medications with their categories >>

# Post-Tagging

```{r}
# load restful_get_rxnorm.py output file
abcd_med_class <- read_csv("abcd_med_class.csv")
```

Exploration to see how the medications are categorized at each level:

```{r}
abcd_med_class %>%
  filter(lvl_2_name == 'PSYCHOANALEPTICS') %>%
  distinct(lvl_4_name, .keep_all = TRUE)
```

# Create New Columns for ABCD Data

In this next section, we will create last 2 week and last 24 hours indicator columns for different medication categories. The data cleaning for this is a little tedious because of how the raw data is organized. Between Y2 and Y3, a wording change was introduced to ask about past year medications used rather than just past 2 weeks. An additional variable was introduced alongside the wording change to indicate whether each medication had been used in the last 2 weeks. These two columns will need to be consolidated for Y3 and after. Last 24 hour use is asked at all waves.

## Data Cleaning

```{r}
# select relevant columns
meds <- ph_p_meds %>%
  select(src_subject_id, eventname, 
         contains('_rxnorm_p'), contains('_rxnorm_1yr_p'), # med name columns
         contains('_2wk_p'), contains('_24')) %>% # med indicator columns
  select(-contains('otc'), -contains('caff')) # exclude over the counter and caffeine
```

The columns with the '2wk_p' suffix are the post-Y2 indicator columns of whether the participant used a medication in the last two weeks. In this next step, we revise the '2wk' columns to have medication names at all waves for medications taken in the last two weeks.

```{r}
meds_rev <- meds %>%
  # if 2wk column has a 1, replace cell with med name from rxnorm_1yr column
  mutate(across(contains('_2wk_'), 
                ~if_else(. == 1, 
                         get(paste0('med', parse_number(cur_column()), '_rxnorm_1yr_p')), NA))) %>%
  # for before wave 3, copy rxnorm column data into 2wk columns
  mutate(across(contains('_2wk_'), 
                ~if_else(parse_number(eventname) < 3, 
                         get(paste0('med', parse_number(cur_column()), '_rxnorm_p')), .)))
```

Next, we'll perform similar process to consolidate medications used in the last 24 hours. We'll pulling medication names from the 'rxnorm' and 'rxnorm_1yr' columns to replace the 1s in the '24' columns, and then setting all other values to NA.

```{r}
meds_rev <- meds_rev %>%
  # for each of the 24 hr columns
  mutate(across(contains('_24'), 
                ~case_when(
                  # pull med names from rxnorm columns if before Y3
                  . == 1 & parse_number(eventname) < 3 ~ 
                    get(paste0('med', parse_number(cur_column()), '_rxnorm_p')),
                  # pull med names from rxnorm_1yr columns if after Y3
                  . == 1 & parse_number(eventname) >= 3 ~ 
                    get(paste0('med', parse_number(cur_column()), '_rxnorm_1yr_p')),
                  # replace all other values with NA
                  TRUE ~ NA)))
```

Now we have just the series of '2wk' and the '24' columns that have the named medications taken in the last 2 weeks and the last 24 hours. This will make it easier to categorize the types of medications people have taken in those timeframes in the next part.

## Creating Category Columns

We will now be going back to the parsed medication dataframe (abcd_med_class). In this example, I want to create dummy-coded columns for each of the psychoanaleptics categories at the level 4 level to indicate whether that category of medication has been taken in the last 2 weeks or 24 hours. These 7 categories are output below:

```{r}
med_types <- abcd_med_class %>%
  filter(lvl_2_name == 'PSYCHOANALEPTICS') %>%
  distinct(lvl_4_name) %>%
  pull(lvl_4_name)
med_types
```

Create a vector of what the new column names prefixes will be (in the same order as above)

```{r}
med_type_abbrv <- c('sympath_adhd', 'ssri_dep', 'other_dep', 'nsri_dep', 'xanth_adhd',
                    'comb_dep_adhd', 'other_adhd')
```

For each medication category, we will create an object for it with the same name as the corresponding column prefix, and the object will contain all the specific medication names that fall into that category.

```{r}
for (i in 1:7) { # for each med category
  med_names <- abcd_med_class %>%
    filter(lvl_4_name == med_types[i]) %>% # filter med tagging df for meds from that category
    mutate(med_full = str_split_i(med_full, " ", 2) %>% str_to_title()) %>% # standardize med names
    distinct(med_full) %>% # condense to unique med names
    pull(med_full) # convert to vector
  
  assign(med_type_abbrv[i], med_names) # assign vector of med names to med category
}
```

Example of the SSRI object, which contains SSRI medication names:

```{r}
ssri_dep
```

Next, run the following for loops. For each medication category and timeframe, it will create a new column that checks whether each row contains a medication name from that category: yes - 1, no - 0. 

```{r}
# create 2wk columns
for (i in 1:7) {
  meds_rev[, paste0(med_type_abbrv[i], '_2wk')] <- apply(meds_rev %>% select(contains('_2wk_')), 1, function(x) as.integer(any(grep(paste(get(med_type_abbrv[i]),collapse="|"),x))))
}
```

```{r}
# create 24 hr columns
for (i in 1:7) {
  meds_rev[, paste0(med_type_abbrv[i], '_24')] <- apply(meds_rev %>% select(contains('_24')), 1, function(x) as.integer(any(grep(paste(get(med_type_abbrv[i]),collapse="|"),x))))
}
```

Once it's done running, you can take out the original medication columns and reorder the variables if you choose.

```{r}
dep_adhd_meds <- meds_rev %>%
  select(src_subject_id:rx_med15_24,
         ssri_dep_2wk, nsri_dep_2wk, other_dep_2wk, comb_dep_adhd_2wk,
         sympath_adhd_2wk, xanth_adhd_2wk, other_adhd_2wk,
         ssri_dep_24, nsri_dep_24, other_dep_24, comb_dep_adhd_24,
         sympath_adhd_24, xanth_adhd_24, other_adhd_24)
```

```{r}
# save as new table
write_csv(dep_adhd_meds, 'abcd_dep_adhd_meds_summary.csv')
```

## Creating Med Name Columns

If you want columns that are more granular, here's an example of creating columns for each of the different medications in a certain category. For our example, we're using SSRIs and for the last two weeks.

```{r}
# filter for med data for 2wk columns and standardize the med names
meds_2wk <- dep_adhd_meds %>%
  select(src_subject_id, eventname, contains('2wk')) %>%
  select(-contains('dep'), -contains('adhd')) %>%
  mutate(across(contains('med'), ~str_split_i(., " ", 2) %>% str_to_title()))
```

```{r}
ssri_dep
```

Because the list of medications includes the scientific and brand name for the same medication (e.g. Prozac = Fluoxetine), we will have to manually account for this when we create the new columns for each medication.

```{r}
dep_meds_2wk <- meds_2wk %>%
  mutate(
    prozac = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Prozac|Fluoxetine', x)))),
    zoloft = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Zoloft|Sertraline', x)))),
    lexapro = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Lexapro|Escitalopram', x)))),
    celexa = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Celexa|Citalopram', x)))),
    luvox = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Luvox|Fluvoxamine', x)))),
    paxil = apply(meds_2wk, 1, function(x)
    as.integer(any(grep('Paxil|Paroxetine', x))))
    ) %>%
  select(src_subject_id, eventname, prozac:paxil)
```

```{r}
dep_meds_2wk
```
