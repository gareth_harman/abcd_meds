# ABCD Medication Parsing

This tool provides logic and scripts for parsing classes of medications within the ABCD Dataset

### Steps:

1) Download or make a copy of this repo.

2) Follow the instructions in abcd-meds.Rmd to pull the necessary columns in the ABCD release data (5.1) for parsing. You can edit the code in this document to fit your specific needs.

Medication suffixes of column names
- _rxnorm_p 
- _rxnorm_1yr_p

3) Run the parse_meds.R script to output a .csv of all unique medications listed in the ABCD data release.

4) Go to the next section of the abcd-meds.Rmd file for a couple minor data cleaning steps before running the next script.

5) Run restful_get_rxnorm.py.  

6) Look at the rest of the abcd-meds.Rmd file for steps to generate useful columns by integrating the parsed medication information with the raw ABCD data.

`parse_meds.R`  
- Requires an .RDS file that contains relevant medication columns from the ABCD data release
- Outputs "medication_tagging.csv" a file containing all unique medication IDs that exist within this .RDS file

`restful_get_rxnorm.py`  
- Calls the rxNorm REST API to grab information from the NLM/NIH rxNorm database to obtain classes of medications
- Requires "medication_tagging.csv", generated from `parse_meds.R`

### Notes:  

- The logic for this medication parsing is consistent with that of an in development tool created by Hauke Bartsch
- Thanks to Hauke for his help in working with the rxNorm database
- For questions email me (Gareth) @ harmang (at) ohsu (dot) edu
- The structure of levels in the output 

### Structure

- The medication levels are organized as follows 

| Level        | Ex.           | Ex.  |
| ------------- |:-------------:| -----:|
| 1      | Broadest category | RESPIRATORY SYSTEM |
| 2      | ...      | DRUGS FOR OBSTRUCTIVE AIRWAY DISEASES |
| 3      | ...     | ADRENERGICS, INHALANTS |
| 4 | Finest category      | Selective beta-2-adrenoreceptor agonists |

